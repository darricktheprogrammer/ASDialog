ASDialog
========

Easily create custom, multi-input dialogs in Applescript.

Originally built as an open source replacement for 24U's Appearance OSAX, ASDialog allows you to add typically inputs one at a time to a dialog box, then display the whole thing at once to a user. Much better than Applescript's built in dialogs with single inputs.

ASDialog is built on [simplegui][1] and [PAsty-Bridge][2] and requires Python 3.

**Example:**
```applescript
property dialogLib : load script file "Path:to:ASDialog:ASDialog.scpt"

-- Because script library files can be located anywhere,
-- you must tell ASDialog where it is located, so it can call its
-- support files.
set libpath to "Path:to:ASDialog:"

set msg to "A simple demo dialog. Here are a few options for asking users (or yourself) for interactive input."

set d to dialogLib's NewDialog(msg, libpath)
d's add_title("Demo dialog")
d's add_dropdown({"Sweet dropdown", "I've seen better"}, "Look at this dropdown", "Sweet dropdown")
d's add_text_field("Your favorite color:", "chartreuse")
d's add_separator() -- separate sections without a label

d's add_radio_buttons({"a", "b", "c"}, "pick a letter", "a")
d's add_checkbox("You like?", false)
d's add_separator_with_label("Separate sections with a label")

d's add_buttons({"More options...", "Stop", "Process"}, "Process", "Stop")
d's display()
--> {buttonReturned:"Process", values:{"Sweet dropdown", "chartreuse", "a", false}}
```

The above code produces the following dialog:

![Dialog demo](docs/img/basic_dialog.png "Dialog demo")


[1]: https://github.com/darricktheprogrammer/simplegui
[2]: https://github.com/darricktheprogrammer/PAsty-Bridge
