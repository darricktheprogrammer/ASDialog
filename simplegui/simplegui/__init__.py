"""
A Library for easier creation of simple dialogs.

@author Darrick Herwehe <darrick@exitcodeone.com>
"""
__author__ = """Darrick Herwehe"""
__email__ = "darrick@exitcodeone.com"
__version__ = "1.1.0"


from . import simplegui
