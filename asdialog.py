#!/usr/bin/python
"""Applescript wrapper which allows access to the simplegui python module."""

import sys
import subprocess


# dynamically import package per PEP 366 to get relative imports.
# https://stackoverflow.com/a/6655098
if __name__ == "__main__" and __package__ is None:
	from pathlib import Path
	package_root = Path(__file__).parents[1]
	sys.path.insert(1, str(package_root))
	import ASDialog
	__package__ = str("ASDialog")


from pastybridge import astranslate as translate
from .simplegui.simplegui import simplegui


#
# Global values for debugging
#
debugging = False
debug_msg = "Hello World"
debug_title = "Window Title"
# debug_widgets = [
# 	"<name=textField><label=Enter some text>",
# 	"<name=radioButtons><choices={1|2|3}><label=radio choice>",
# 	"<name=buttons><buttons={b1|b2}>",
# ]
debug_widgets = [
	"<name=buttons><buttons={Cancel|OK}><okButton=OK><cancelButton=Cancel>",
	"<name=checkbox><label=A checkbox><checked=False>",
	"<name=checkbox><label=Another checkbox><checked=True>",
]


def bring_dialog_to_front():
	# Using shell=True allows script to continue instead of hanging.
	subprocess.run(
		["osascript", "-e", 'tell app "Python" to activate'], shell=True
	)


def add_widget(d, widget):
	widgetName = widget.pop("name")

	if widgetName == "buttons":
		bList = widget.pop("buttons")
		d.add_buttons(bList, **widget)
	elif widgetName == "checkbox":
		label = widget.pop("label")
		widget["checked"] = translate.PBbool.to_python(widget["checked"])
		d.add_checkbox(label, **widget)
	else:
		dialogRoutines = {
			"dropdown": d.add_dropdown,
			"textField": d.add_text_field,
			"radioButtons": d.add_radio_buttons,
			"separator": d.add_separator,
		}
		dialogRoutines[widgetName](**widget)
	return d


def unpack_list_values(dict):
	for k, v in dict.items():
		if len(v) > 0 and (v[0], v[-1]) == ("{", "}"):
			dict[k] = translate.PBlist.to_python(v)
	return dict


def main():
	msg, title, widgets = (
		(debug_msg, debug_title, debug_widgets)
		if debugging
		else (sys.argv[1], sys.argv[2], sys.argv[3:])
	)

	for i, w in enumerate(widgets):
		widgetProperties = translate.PBdict.to_python(w)
		widgets[i] = unpack_list_values(widgetProperties)

	d = simplegui.Dialog(msg, title=title)
	for w in widgets:
		d = add_widget(d, w)

	bring_dialog_to_front()
	print(translate.PBdict.to_applescript(d.display()))


if __name__ == "__main__":
	main()
